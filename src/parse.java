import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by Ian on 3/28/2017.
 */
public class parse {
    public static int states; // number of states
    public static int startingState; //starting state
    public static ArrayList<Integer> acceptingStates = new ArrayList<Integer>(); //arraylist for accepting states
    public static ArrayList<String> characters = new ArrayList<String>(); // arraylist of acceptable characters
    public static ArrayList<String> transitions = new ArrayList<String>(); //arraylist of all transitions, before being put into sets
    public static ArrayList<String> input = new ArrayList<String>();
    public static String filename;
    public int stateNumber = 0;
    public String newState;
    public ArrayList<Integer> currentState = new ArrayList<Integer>();
    public Map<String, ArrayList> transMap = new HashMap<String, ArrayList>(); //maps character to it's transition states
    public ArrayList<Integer> transStates; //arraylist of transition states
    public ArrayList<Map> mapAL = new ArrayList<Map>(); // arraylist of the map for each state
    public ArrayList<String> newStates = new ArrayList<String>();
    public ArrayList<Integer> closureAL = new ArrayList<Integer>();
    public ArrayList<String> unCheckedStates = new ArrayList<String>();
    public ArrayList<String> checkedStates = new ArrayList<String>();
    public Map<String, Integer> stateMap = new HashMap<String, Integer>();
    public ArrayList<Integer> dfaAcceptingStates = new ArrayList<Integer>();
    public ArrayList<Integer> dfaList;
    public List<List<Integer>> listOfLists = new ArrayList<List<Integer>>();
    public Map<String, Integer> listMap = new HashMap<String, Integer>();
    public ArrayList<String> acceptedStrings = new ArrayList<String>();
    public ArrayList<Tuple> tList = new ArrayList<Tuple>();
    public ArrayList<Tuple> markedList = new ArrayList<Tuple>();
    public ArrayList<Tuple> unmarkedList = new ArrayList<Tuple>();
    public ArrayList<String> minList = new ArrayList<String>();
    public ArrayList<Integer> lastList;
    public List<List<Integer>> listOfLists2 = new ArrayList<List<Integer>>();
    public ArrayList<Integer> minAcceptingStates = new ArrayList<Integer>();


    public void printDFA() {
        System.out.print("\nMinimized DFA from " + filename + ":\n");
        System.out.print("Sigma: ");
        for (int i = 0; i < characters.size(); i++) {
            System.out.print(characters.get(i) + " ");
        }
        System.out.print("\n-------------\n");
        for (int i = 0; i < minList.size(); i++) {
            System.out.print(i + ":     ");
            for (int j = 0; j < characters.size(); j++) {
                System.out.print(listOfLists2.get(i).get(listMap.get(characters.get(j))) + " ");
            }
            System.out.print("\n");
        }
        System.out.print("-------------\n");
        System.out.print(startingState + ": Initial State\n");
        if (acceptingStates.size() == 1) {
            System.out.print(minAcceptingStates.get(0));
        } else {
            int count = 0;
            for (int i = 0; i < minAcceptingStates.size() - 1; i++) {
                System.out.print(minAcceptingStates.get(i) + ", ");
                count++;
            }
            System.out.print(minAcceptingStates.get(count));
        }
        System.out.print(": Accepting States\n");
    }

    public void closure(int state) {
        for (int j = 0; j < closureAL.size(); j++) {
            if (state == closureAL.get(j)) {
                return;
            }
        }
        closureAL.add(state);
        transMap = mapAL.get(state);
        if (transMap.get("LAMBDA") != null) { // check if LAMBDA is key in the map
            ArrayList<Integer> tempAL = transMap.get("LAMBDA"); //sets a temp arraylist of states that are mapped to LAMBDA
            if (tempAL.get(0) != -1) {
                for (int k = 0; k < tempAL.size(); k++) { // for amount of states mapped to LAMBDA
                    closure(tempAL.get(k));
                }
            }
        }
        String s = "";
        for (int j = 0; j < closureAL.size(); j++) {
            s += closureAL.get(j) + " ";
        }
        newState = s;
    }

    public String sort(String s) {
        StringTokenizer st = new StringTokenizer(s);
        ArrayList<Integer> tempAL = new ArrayList<Integer>();
        while (st.hasMoreElements()) {
            String temp = st.nextToken(" ");
            tempAL.add(Integer.parseInt(temp));
        }
        Collections.sort(tempAL);
        s = "";
        for (int k = 0; k < tempAL.size(); k++) {
            s += tempAL.get(k) + " ";
        }
        return s;
    }

    public void mapChar() {
        for (int i = 0; i < characters.size(); i++) {
            listMap.put(characters.get(i), i);
        }
    }

    public ArrayList<Integer> transition(ArrayList<Integer> state, String c) {
        ArrayList<Integer> transAL = new ArrayList<Integer>();
        for (int i = 0; i < state.size(); i++) {
            transMap = mapAL.get(state.get(i));
            if (transMap.get(c) != null) { //check for mapping with character as key
                ArrayList<Integer> tempAL = transMap.get(c); // sets tempAL to arraylist of states that was mapped to character
                if (tempAL.get(0) != -1) { //checks that state or states exist
                    for (int j = 0; j < tempAL.size(); j++) { //loop for number of states mapped to character
                        transAL.add(tempAL.get(j)); // go to next state that is linked from the current state
                    }
                }
            }
        }
        return transAL;
    }

    public void toDFA() {
        closure(startingState);
        closureAL.clear();
        newState = sort(newState);
        newStates.add(newState);
        checkedStates.add(newState);
        stateMap.put(newStates.get(0), stateNumber);
        StringTokenizer st = new StringTokenizer(newState);
        while (st.hasMoreElements()) {
            String temp = st.nextToken(" ");
            currentState.add(Integer.parseInt(temp));
        }
        while (currentState != null) {
            dfaList = new ArrayList<Integer>();
            for (int i = 0; i < characters.size(); i++) { // for amount of characters
                String s = "";
                ArrayList<Integer> temp = transition(currentState, characters.get(i)); //gets transition states
                for (int j = 0; j < temp.size(); j++) { //for num of transition states
                    closure(temp.get(j)); //find closure
                    closureAL.clear();
                    st = new StringTokenizer(newState);
                    ArrayList<String> tempAL = new ArrayList<String>();
                    while (st.hasMoreElements()) {
                        String temp2 = st.nextToken(" ");
                        tempAL.add(temp2);
                    }
                    boolean isIn = false;
                    for (int k = 0; k < tempAL.size(); k++) {
                        for (int y = 0; y < s.length(); y++) {
                            if (tempAL.get(k).charAt(0) == s.charAt(y)) {
                                isIn = true;
                            }
                        }
                        if (isIn == false) {
                            s += tempAL.get(k) + " ";
                        }
                    }
                }
                s = sort(s);
                boolean isIn = false;
                for (int j = 0; j < newStates.size(); j++) { //check if state already exists
                    if (s.equals(newStates.get(j))) {
                        isIn = true;
                    }
                }
                if (!isIn) {
                    newStates.add(s);
                    stateNumber++;
                    stateMap.put(newStates.get(stateNumber), stateNumber);
                }
                isIn = false;
                for (int j = 0; j < checkedStates.size(); j++) { //check if state was already Checked
                    if (s.equals(checkedStates.get(j))) {
                        isIn = true;
                    }
                }
                for (int j = 0; j < unCheckedStates.size(); j++) { //check if state is in the check queue
                    if (s.equals(unCheckedStates.get(j))) {
                        isIn = true;
                    }
                }
                if (!isIn) {
                    unCheckedStates.add(s);
                }
                String xyz = "";
                for (int z = 0; z < currentState.size(); z++) {
                    xyz += currentState.get(z) + " ";
                }
                dfaList.add(stateMap.get(s));
                listOfLists.add(stateMap.get(xyz), dfaList);
            }
            currentState.clear();
            if (!unCheckedStates.isEmpty()) {
                st = new StringTokenizer(unCheckedStates.get(0));
                while (st.hasMoreElements()) {
                    String temp = st.nextToken(" ");
                    currentState.add(Integer.parseInt(temp));
                }
                checkedStates.add(unCheckedStates.get(0));
                unCheckedStates.remove(0);
            }
            //set accepting states
            if (unCheckedStates.isEmpty() && currentState.isEmpty()) {
                for (int i = 0; i < acceptingStates.size(); i++) {
                    for (int j = 0; j < newStates.size(); j++) {
                        st = new StringTokenizer(newStates.get(j));
                        while (st.hasMoreElements()) {
                            String temp = st.nextToken(" ");
                            int k = Integer.parseInt(temp);
                            if (acceptingStates.get(i) == k) {
                                boolean isIn = false;
                                for (int w = 0; w < dfaAcceptingStates.size(); w++) {
                                    if (stateMap.get(newStates.get(j)) == dfaAcceptingStates.get(w)) {
                                        isIn = true;
                                    }
                                }
                                if (!isIn) {
                                    dfaAcceptingStates.add(stateMap.get(newStates.get(j)));
                                    break;
                                }
                            }
                        }
                    }
                }
                break;
            }
        }
    }

    public void toHashMap() {
        int k = 0;
        for (int i = 0; i < states; i++) {
            transMap = new HashMap<String, ArrayList>();
            for (int j = 0; j <= characters.size(); j++) {
                transStates = new ArrayList<Integer>();
                String temp = transitions.get(k);
                int w;
                StringTokenizer st = new StringTokenizer(temp);
                while (st.hasMoreTokens()) {
                    try {
                        String temp2 = st.nextToken("{,}");
                        w = Integer.parseInt(temp2);
                    } catch (NoSuchElementException nse) {
                        w = -1;
                    }
                    transStates.add(w);
                }
                if (j < characters.size()) {
                    transMap.put(characters.get(j), transStates);
                } else {
                    transMap.put("LAMBDA", transStates);
                }
                k++;
            }
            mapAL.add(i, transMap);
        }
    }

    public void inputString() {
        for (int i = 0; i < input.size(); i++) { // for amount of input
            String s = "";
            int place = startingState;
            for (int j = 0; j < input.get(i).length(); j++) { //for length of input
                for (int k = 0; k < characters.size(); k++) { //for available characters
                    if (String.valueOf(input.get(i).charAt(j)).equals(characters.get(k))) {
                        place = listOfLists2.get(place).get(listMap.get(characters.get(k)));
                        s += characters.get(k);
                    }
                }
            }
            if (input.get(i).equals(s)) {
                for (int j = 0; j < minAcceptingStates.size(); j++) {
                    if (place == minAcceptingStates.get(j)) {
                        acceptedStrings.add(s);
                        break;
                    }
                }
            }
        }
        System.out.println("\nThe following strings are accepted:");
        for (int i = 0; i < acceptedStrings.size(); i++) {
            System.out.println(acceptedStrings.get(i));
        }
    }

    public void table() {
        for (int i = 0; i < newStates.size(); i++) {
            for (int j = 0; j < newStates.size(); j++) {
                boolean isIn = false;
                boolean x = false;
                boolean y = false;
                if (i != j) {
                    Tuple t = new Tuple(0, 0);
                    if (i < j) {
                        t = new Tuple(i, j);
                    }
                    if (i > j) {
                        t = new Tuple(j, i);
                    }
                    for (int k = 0; k < tList.size(); k++) {
                        if (t.x == tList.get(k).x && t.y == tList.get(k).y) {
                            isIn = true;
                        }
                    }
                    if (!isIn) {
                        tList.add(t);
                        for (int k = 0; k < dfaAcceptingStates.size(); k++) {
                            if (t.x == dfaAcceptingStates.get(k)) {
                                x = true;
                            }
                            if (t.y == dfaAcceptingStates.get(k)) {
                                y = true;
                            }
                        }
                        if (x && !y) {
                            markedList.add(t);
                        } else if (!x && y) {
                            markedList.add(t);
                        }
                    }
                }
            }
        }
    }

    public void minimize() {
        while (true) {
            int markSize = markedList.size();
            for (int i = 0; i < tList.size(); i++) {
                boolean isMarked = false;
                for (int j = 0; j < markedList.size(); j++) {
                    if (tList.get(i) == markedList.get(j)) {
                        isMarked = true;
                    }
                }
                if (!isMarked) {
                    unmarkedList.add(tList.get(i));
                }
            }
            for (int i = 0; i < unmarkedList.size(); i++) {
                int tempState1;
                int tempState2;
                for (int j = 0; j < characters.size(); j++) {
                    tempState1 = listOfLists.get(unmarkedList.get(i).x).get(listMap.get(characters.get(j)));
                    tempState2 = listOfLists.get(unmarkedList.get(i).y).get(listMap.get(characters.get(j)));
                    boolean mark = false;
                    for (int k = 0; k < markedList.size(); k++) {
                        if (tempState1 == markedList.get(k).x && tempState2 == markedList.get(k).y) {
                            mark = true;
                        }
                    }
                    if (mark) {
                        markedList.add(unmarkedList.remove(i));
                        i--;
                        break;
                    }
                }
            }
            if (markSize == markedList.size()) {
                break;
            }
        }
    }

    public void createMin() {
        for (int i = 0; i < newStates.size(); i++) {
            minList.add(String.valueOf(i) + " ");
        }
        for (int g = 0; g < unmarkedList.size(); g++) {
            for (int i = 0; i < minList.size(); i++) {
                boolean xyz = false;
                boolean x = false;
                boolean y = false;
                StringTokenizer st = new StringTokenizer(minList.get(i));
                while (st.hasMoreElements()) {
                    String temp = st.nextToken(" ");
                    for (int j = 0; j < temp.length(); j++) {
                        if (Integer.parseInt(String.valueOf(temp.charAt(j))) == unmarkedList.get(g).x || Integer.parseInt(String.valueOf(temp.charAt(j))) == unmarkedList.get(g).y) {
                            xyz = true;

                        }
                    }
                    if (xyz) {
                        for (int j = 0; j < temp.length(); j++) {
                            if (unmarkedList.get(g).x == Integer.parseInt(String.valueOf(temp.charAt(j)))) {
                                x = true;
                            }
                            if (unmarkedList.get(g).y == Integer.parseInt(String.valueOf(temp.charAt(j)))) {
                                y = true;
                            }
                        }
                    }
                    if (x) {
                        String s = minList.get(i);
                        s = add(s, String.valueOf(unmarkedList.get(g).y));
                        minList.remove(i);
                        s = sort(s);
                        minList.add(i, s);
                    }
                    if (y) {
                        String s = minList.get(i);
                        s = add(s, String.valueOf(unmarkedList.get(g).x));
                        minList.remove(i);
                        s = sort(s);
                        minList.add(i, s);
                    }
                }
            }
        }
        condense();
        createMinDFA();
        getStartingAndAccepting();
    }

    public void condense() {
        for (int i = 0; i < minList.size(); i++) {
            for (int j = 0; j < minList.size(); j++) {
                if (i < j) {
                    if (minList.get(i).equals(minList.get(j))) {
                        minList.remove(j);
                        j--;
                    }
                }
            }
        }
    }

    public String add(String x, String y) {
        for (int i = 0; i < y.length(); i++) {
            boolean isIn = false;
            for (int j = 0; j < x.length(); j++) {
                if (y.charAt(i) == x.charAt(j)) {
                    isIn = true;
                }
            }
            if (!isIn) {
                x += y.charAt(i) + " ";
            }
        }

        return x;
    }

    public void createMinDFA() {
        for (int i = 0; i < minList.size(); i++) {
            lastList = new ArrayList<Integer>();
            for (int j = 0; j < characters.size(); j++) {
                boolean found = false;
                if (minList.get(i) != null) {
                    StringTokenizer st = new StringTokenizer(minList.get(i));
                    int k = 0;
                    while (st.hasMoreElements()) {
                        String s = st.nextToken(" ");
                        k = listOfLists.get(Integer.parseInt(s)).get(listMap.get(characters.get(j)));
                        for (int w = 0; w < minList.size(); w++) {
                            for (int p = 0; p < minList.get(w).length(); p++) {
                                if (String.valueOf(k).charAt(0) == minList.get(w).charAt(p)) {
                                    lastList.add(w);
                                    listOfLists2.add(i, lastList);
                                    found = true;
                                    break;
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                        if (found) {
                            break;
                        }
                    }
                }
            }
        }
    }

    public void getStartingAndAccepting() {
        boolean done = false;
        for (int i = 0; i < minList.size(); i++) {
            for (int j = 0; j < minList.get(i).length(); j++) {
                if (Integer.parseInt(String.valueOf(minList.get(i).charAt(j))) == startingState) {
                    startingState = i;
                    done = true;
                    break;
                }
            }
            if (done) {
                break;
            }
        }
        done = false;
        for (int i = 0; i < minList.size(); i++) {
            for (int j = 0; j < minList.get(i).length(); j++) {
                for (int k = 0; k < dfaAcceptingStates.size(); k++) {
                    if (minList.get(i).charAt(j) == String.valueOf(dfaAcceptingStates.get(k)).charAt(0)) {
                        minAcceptingStates.add(i);
                        done = true;
                        break;
                    }
                }
                if (done) {
                    break;
                }
            }
        }

    }

    public class Tuple<X, Y> {
        public final int x;
        public final int y;

        public Tuple(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }

    public static void main(String args[]) {
        parse parse = new parse();
        filename = args[0];
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filename));
            String line;
            line = br.readLine();
            if (line != null) {
                states = Integer.parseInt(line);
            }
            line = br.readLine();
            if (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreElements()) {
                    characters.add(st.nextToken("\t "));
                }
            }
            if (states != 0) {
                for (int i = 0; i < states; i++) {
                    line = br.readLine();
                    if (line != null) {
                        StringTokenizer st = new StringTokenizer(line);
                        st.nextToken("\t ");
                        while (st.hasMoreElements()) {
                            transitions.add(st.nextToken("\t "));
                        }
                    }
                }
            }
            line = br.readLine();
            if (line != null) {
                startingState = Integer.parseInt(line);
            }
            line = br.readLine();
            if (line != null) {
                String temp;
                StringTokenizer st = new StringTokenizer(line);
                while (st.hasMoreElements()) {
                    temp = st.nextToken("\t{}, ");
                    acceptingStates.add(Integer.parseInt(temp));
                }
            }
            parse.toHashMap();
            parse.mapChar();
            parse.toDFA();
            parse.table();
            parse.minimize();
            parse.createMin();
            parse.printDFA();
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file.");
        } catch (IOException e) {
            System.out.println("Could not read file.");
        }
        try {
            br = new BufferedReader(new FileReader(args[1]));
            String line;
            line = br.readLine();
            while (line != null) {
                input.add(line);
                line = br.readLine();
            }
            parse.inputString();
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file.");
        } catch (IOException e) {
            System.out.println("Could not read file.");
        }
    }
}
